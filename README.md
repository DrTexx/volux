| BRANCH  | BUILD STATUS | COVERAGE | REQUIREMENTS | ISSUES | OPEN PRs |
| ---     | :---:        | :---:    | :---:        | :---:  | :---:    |
| Master  | [![Build Status](https://travis-ci.org/DrTexxOfficial/volux.svg?branch=master)](https://travis-ci.org/DrTexxOfficial/volux) | [![coverage report](https://gitlab.com/DrTexx/volux/badges/master/coverage.svg)](https://gitlab.com/DrTexx/volux/commits/master) | [![Requirements Status](https://requires.io/gitlab/DrTexxOfficial/volux/requirements.svg?branch=master)](https://requires.io/gitlab/DrTexxOfficial/volux/requirements/?branch=master) | [![GitHub issues](https://img.shields.io/github/issues/DrTexxOfficial/volux.svg?branch=master)](https://GitHub.com/DrTexxOfficial/volux/issues/) | [![GitHub pull-requests](https://img.shields.io/github/issues-pr/DrTexxOfficial/volux.svg?branch=master)](https://GitHub.com/DrTexxOfficial/volux/pull/) |
| Develop | [![Build Status](https://travis-ci.org/DrTexxOfficial/volux.svg?branch=develop)](https://travis-ci.org/DrTexxOfficial/volux) | [![coverage report](https://gitlab.com/DrTexx/volux/badges/develop/coverage.svg)](https://gitlab.com/DrTexx/volux/commits/develop) | [![Requirements Status](https://requires.io/gitlab/DrTexxOfficial/volux/requirements.svg?branch=develop)](https://requires.io/gitlab/DrTexxOfficial/volux/requirements/?branch=develop)

# Debug Interface - volux 

[![PyPI Version](https://img.shields.io/pypi/v/volux.svg)](https://pypi.python.org/pypi/volux/)
[![GitHub release](https://img.shields.io/github/release-pre/drtexxofficial/volux.svg)](https://GitHub.com/DrTexxOfficial/volux/releases/)
[![GitHub license](https://img.shields.io/github/license/DrTexxOfficial/volux.svg?branch=master)](https://github.com/DrTexxOfficial/volux/blob/master/LICENSE)
[![Github all releases](https://img.shields.io/github/downloads/DrTexxOfficial/volux/total.svg)](https://GitHub.com/DrTexxOfficial/volux/releases/)

## Installation
### Requirements

    $ sudo apt-get install python3-tk python3-xlib python3-dbus libasound2-dev

### Install via pip
Install as user (recommended):

    $ pip3 install volux --user

Install as root:

    $ sudo pip3 install volux

### Install via wheel (.whl)
Install as user (recommended):

    $ pip3 install volux-*-py3-none-any.whl --user
    
Install as root:

    $ sudo pip3 install volux-*-py3-none-any.whl

### Build from source
Clone this repository:

    $ git clone https://github.com/DrTexxOfficial/volux.git

Install pip requirements:

    $ cd volux
    $ pip3 install -r requirements.txt --user

Build:

    $ python3 setup.py bdist_wheel
    
## Script Functionality
## Examples

<br/>

[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
